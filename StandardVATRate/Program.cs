﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using StandardVATRate.Business.Service.Abstractions;
using StandardVATRate.Business.Service.Implementations;
using System;

namespace StandardVATRate
{
    class Program
    {
        static void Main(string[] args)
        {      

            // instantiate DI and configure logger
            var serviceProvider = new ServiceCollection()
                       .AddSingleton<IOperation, Operation>()
                       .AddLogging(cfg => cfg.AddConsole()).Configure<LoggerFilterOptions>(cfg => cfg.MinLevel = LogLevel.Debug)
                       .BuildServiceProvider();

            // get instance of logger
            var logger = serviceProvider.GetService<ILogger<Program>>();     

            logger.LogInformation("Starting application");

            //get operation service instance
            var operation = serviceProvider.GetService<IOperation>();

            var highestStandards = operation.GetTheThreeHighestStandardVATRateCountries();

            if (highestStandards != null)
            {
                foreach (var item in highestStandards)
                {
                    Console.WriteLine("The highest standard VAT rate : {0}-{1} ", item.Country, item.Standard);
                }
            }

            var lowestStandards = operation.GetTheThreeLowestStandardVATRateCountries();

            if (lowestStandards != null)
            {

                foreach (var item in lowestStandards)
                {
                    Console.WriteLine("The lowest standard VAT rate : {0}-{1} ", item.Country, item.Standard);
                }
            }

            logger.LogInformation("Done!");

            Console.ReadKey();

        }
    }
}
