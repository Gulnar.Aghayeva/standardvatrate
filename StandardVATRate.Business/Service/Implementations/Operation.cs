﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Converters;
using StandardVATRate.Business.Models;
using StandardVATRate.Business.Service.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace StandardVATRate.Business.Service.Implementations
{
    public class Operation : IOperation
    {
        private readonly ILogger _logger;

        public Operation(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<Operation>();
        }


        private string GetJsonObject()
        {
            _logger.LogInformation("Getting json object from given address");

            string json;

            using (WebClient wc = new WebClient())
            {
                json = wc.DownloadString("http://jsonvat.com/");
            }
            return json;
        }

        public VATRateInformation ConvertJsonObject()
        {
            VATRateInformation result = new VATRateInformation();

            try
            {
                string json = GetJsonObject();

                _logger.LogInformation("Converting json object to c# object ");

                result = Newtonsoft.Json.JsonConvert.DeserializeObject<VATRateInformation>(json);

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occured : {ex}");
            }
            return result;
        }

        List<CountryWithStandardVATRate> IOperation.GetTheThreeHighestStandardVATRateCountries()
        {
            VATRateInformation rates = ConvertJsonObject();

            _logger.LogInformation("Getting the higgest three standard with country name");

            var result = rates.Rates?.Select(p => new CountryWithStandardVATRate
            {
                Country = p.Name,
                Standard = p.Periods.OrderByDescending(k => k.EffectiveFrom).Select(k => k.Rate.Standard).Take(1).FirstOrDefault()

            })
            .OrderByDescending(x => x.Standard)
            .Take(3)
            .ToList();

            return result;
        }

        List<CountryWithStandardVATRate> IOperation.GetTheThreeLowestStandardVATRateCountries()
        {
            VATRateInformation rates = ConvertJsonObject();

            _logger.LogInformation("Getting the lowest three standard with country name");

            var result = rates.Rates?.Select(p => new CountryWithStandardVATRate
            {
                Country = p.Name,
                Standard = p.Periods.OrderByDescending(k => k.EffectiveFrom).Select(k => k.Rate.Standard).Take(1).FirstOrDefault()

            })
          .OrderBy(x => x.Standard)
          .Take(3)
          .ToList();

            return result;
        }
    }
}
