﻿using StandardVATRate.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StandardVATRate.Business.Service.Abstractions
{
    public interface IOperation
    {
        VATRateInformation ConvertJsonObject();
        List<CountryWithStandardVATRate> GetTheThreeHighestStandardVATRateCountries();
        List<CountryWithStandardVATRate> GetTheThreeLowestStandardVATRateCountries();
    }
}
