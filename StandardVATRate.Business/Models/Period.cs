﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace StandardVATRate.Business.Models
{
    public class Period
    {       

        [JsonProperty("effective_from")]
        public string EffectiveFrom { get; set; }

        [JsonProperty("rates")]
        public Rate Rate { get; set; }
    }
}


