﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandardVATRate.Business.Models
{
    public class CountryWithStandardVATRate
    {
        public string Country { get; set; }
        public double Standard { get; set; }
    }
}
