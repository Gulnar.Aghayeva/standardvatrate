﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StandardVATRate.Business.Models
{
    public class CountryVATRateInformation
    {
        public string Name { get; set; }
        public string Code { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        public List<Period> Periods { get; set; }
    }
}
