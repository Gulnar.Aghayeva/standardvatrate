﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StandardVATRate.Business.Models
{
    public class VATRateInformation
    {
        [JsonProperty("details")]
        public string Detail { get; set; }

        public string Version { get; set; }

        public List<CountryVATRateInformation> Rates { get; set; }
    }
}
