﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandardVATRate.Business.Models
{
    public class Rate
    {
        public Nullable<double> SuperReduced { get; set; }
        public Nullable<double> Reduced1 { get; set; }
        public Nullable<double> Reduced2 { get; set; }
        public double Standard { get; set; }
        public Nullable<double> Parking { get; set; }
    }
}
